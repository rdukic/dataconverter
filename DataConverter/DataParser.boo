﻿namespace DataConverter

import System

class DataParser:
	
	
	public def constructor():
		pass
		
	public def TryParseDouble(inputString as string) as double:
		i = double.MinValue
		try:
			inputString = inputString.Replace(".",",")
			i = double.Parse(inputString)
		except e as InvalidCastException:
			print "InvalidCastException: $inputString cannot be parsed to double."
		except e as FormatException:
			print "FormatException: $inputString cannot be parsed to double."
		return i
		
	public def TryParseDate(inputString as string) as date:
		i = date(1970,1,1)
		try:
			i = DateTime.Parse(inputString)			
		except e as InvalidCastException:
			print "InvalidCastException: $inputString cannot be parsed to date."
		except e as FormatException:
			print "FormatException: $inputString cannot be parsed to date."
		return i
	
	public def CanOnlyBeString(inputString as string) as bool:
		if TryParseDouble(inputString) == double.MinValue and TryParseDate(inputString).Equals(date(1970,1,1)):
			return true
		else:
			return false
						
	public def GenerateValuesList(readValues as Array) as List[of string]:
		result = List[of string]()
		
		using d = DataConverter():
		
			for value in readValues:
				if CanOnlyBeString(value):
					value = d.ConvertTextToUpper(value)
					result.Add(value)
					continue
					
				testDouble = TryParseDouble(value)
				if not testDouble == double.MinValue:
					//if the data is double then force decimal separator to be the dot because otherwise it gets added to the list as two separate values.
					testDouble = d.ConvertTempToF(testDouble)
					result.Add(testDouble.ToString().Replace(",","."))
					continue
					
				testDate = TryParseDate(value)
				if not testDate == date(1970,1,1):
					//if the data is a date then format the string so it's acceptable for PostgreSQL
					testDate = d.ConvertDateToUtc(testDate)
					result.Add(testDate.ToString("yyyy-MM-dd HH:mm:ss"))
					
			return result
			
				
				
			