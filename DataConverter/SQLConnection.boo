﻿namespace DataConverter

import System
import Npgsql

class SQLConnection: 
	
	private static dbConnection as NpgsqlConnection

	public def constructor(connectionString as string):
		dbConnection = NpgsqlConnection(connectionString)

	public def OpenConnection():
		try:
			dbConnection.Open()
		except e as NpgsqlException:
			print "Exception thrown! $(e.Message)"
			
	public def CloseConnection():
		try:
			dbConnection.Close()
		except e as NpgsqlException:
			print "Exception thrown! $(e.Message)"
			

	//function to fetch columns from the postgreSQL table. Meant for checking if there aren't any extra columns in the CSV file.

	public static def FetchColumns() as List[of string]:		
		columns = List[of string]()
		try:
			sql = "SELECT column_name FROM information_schema.columns WHERE table_schema = 'public' AND table_name='information';"
			dbCommand = dbConnection.CreateCommand()
			dbCommand.CommandText = sql
			reader = dbCommand.ExecuteReader()
			
			while reader.Read():
				colName = reader["column_name"]
				columns.Add(colName)
				
		except e as NpgsqlException:
			print "NpgsqlException thrown: $(e.Message)"		
		return columns
		
	//function for inserting processed data in the postgreSQL table	
		
	public static def InsertRow(columns as Array, values as List[of string]) as bool:

		completed = false

		try:

			sql = BuildSQLInsertQuery(columns, values)
	
			dbCommand = dbConnection.CreateCommand()
			dbCommand.CommandText = sql
			writer = dbCommand.ExecuteReader()
			writer.Close()
			
			completed = true
						
		except e as NpgsqlException:
			print "NpgsqlException thrown: $(e.Message)"
			
		return completed
		

	private static def BuildSQLInsertQuery(columns as Array, values as List[of string]) as string:
		using dataParser = DataParser():

			useQuotes = false	//If the value isn't a string or a date then the quotes shouldn't be added to the query text	
			lastElement = false
			//building query string

			sql = "INSERT INTO information("
			for i in range(columns.Length):			
				if i == columns.Length - 1:
					sql += columns[i] + ")"
				else:
					sql += columns[i] + ","
			
			sql += " VALUES("
			
			for val in values:
				lastElement = values.IndexOf(val) == values.Count - 1								
				
				if not dataParser.TryParseDouble(val) == double.MinValue:
					useQuotes = false
				else:
					useQuotes = true 
				
				if lastElement:
					if useQuotes:
						sql += "'" + val + "')"
					else:
						sql += val + ")"
				else:
					if useQuotes:
						sql += "'" + val + "',"
					else:
						sql += val + ","
						
		return sql
