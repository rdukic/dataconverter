﻿namespace DataConverter

import System
import System.IO
import Npgsql

class DataConverter:

	public def constructor():
		pass
		
	public def ConvertTempToF(temp as double) as double:
		return temp * 1.8 + 32
		
	public def ConvertTempToC(temp as double) as double:
		return (temp - 32) / 1.8		

	public def ConvertTextToUpper(text as string) as string:
		return text.ToUpper()	
	
	public def ConvertTextToLower(text as string) as string:
		return text.ToLower()
		
	public def ConvertDateToUtc(d as date) as date:
		return d.ToUniversalTime()	
		
	public def ConvertDateToLocal(d as date) as date:
		return d.ToLocalTime()
		
	public def ConvertCSVToPostgre(filePath as string, connectionURL as string):
		try:		

			if not File.Exists(filePath):
				raise FileNotFoundException("File not found at the given FilePath")
		
			sqlConn = SQLConnection(connectionURL)
			sqlConn.OpenConnection()
			
			using sr = StreamReader(filePath):
				firstRun = true //on first run, a check for column names is done
				columnOK = true //if this is false, the columns from CSV do not match the ones in the PostgreSQL.
								//Columns in CSV file can contain a subset of columns which are contained in SQL, but shouldn't contain any unique columns.
				
				while csvLine = sr.ReadLine():
					if firstRun == true:
						sqlColumns = SQLConnection.FetchColumns()			
						csvColumns = csvLine.Split(char(','))
		
						//check if all the columns in CSV are present in the PostgreSQL database
						if csvColumns.Length > sqlColumns.Count:		
							print "There are more CSV columns than SQL."
							columnOK = false
							break
		
						for column in sqlColumns:
							if not (column as string).Contains('id'):
								if not csvLine.Contains(column):
									print "All columns must be present in SQL table!. Missing column: $column"
									columnOK = false
									break
		
						firstRun = false
						if not columnOK:
							break
					else:
						if not (csvLine.StartsWith('/') or csvLine.StartsWith('!')) and not csvLine.Equals(""): //Check if the line in the CSV file is a comment and contains text		
							values = csvLine.Split(char(','))
							//convert values read from CSV to other types (double/date)
							
							using dataParser = DataParser():
								convertedValues = dataParser.GenerateValuesList(values)
							
							done = sqlConn.InsertRow(csvColumns, convertedValues)
							
							if done == false:
								print "Latest insert wasn't successful.\nCSV line: $csvLine"
								break
						else:
							print 'Comment line!'
												
			sqlConn.CloseConnection()
			
		except e as NpgsqlException:
			print "NpgsqlException thrown: $(e.Message)"
		except e as FileNotFoundException:
			print "FileNotFoundException thrown: $(e.Message)"
		