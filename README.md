# Data converter #

Application is used to convert data from one type to another. The current version supports conversion from a CSV file to a PostgreSQL entry.

## Requirements

* SharpDevelop 4.4.2 (latest version of SharpDevelop doesn't come with Boo) [link](https://github.com/icsharpcode/SharpDevelop/releases/tag/v5.1-archival)
* PostGreSQL [link](https://www.postgresql.org/)
* Npgsql 2.2.0 (also included in the project) [link](https://www.nuget.org/packages/Npgsql/)

## Description

### Classes

The code is separated into following classes:

- DataParser - contains functions that determine the type of data and a function for converting the values from CSV to a List
- SQLConnection - contains functions that communicate with the PostgreSQL database and a function that builds the INSERT query with processed values.
- DataConverter - contains functions that process values and a function for converting values from CSV file to PostgreSQL
    - double values are treated as temperature in Celsius and are converted to temperature in Fahrenheit,
    - string values are converted so they consist of upper characters,
    - date values are converted to UTC,
    - There are also other functions but are not used (reverse of functions that are used)



## Problems / Thoughts 

Currently all values of a certain type are treated as if they represent the same information. This could be resolved by inspecting the column names and determine the proper processing function or by establishing a protocol, so the processing is done with the given description of each column (all double values are treated as temperature, all string values are converted to upper characters, ...).

In the current version each line is processed and inserted into the database separately. This could be improved by processing information in batches (which is what I'm currently working on, but is not in master branch).

The current version of the application cannot run out of the box since the database isn't included. If you want to try it you must create a database and edit the connectionString variable appropriately. An out of the box version would be possible with the help of Docker.

### Source control

 * master - contains the last stable version,
 * development - meant for minor fixes. Additional functionality and/or bigger fixes will be done in a separate branch and then merged to this one.

### Author

* Rok Dukič - rok1071@gmail.com